# Musala Coding Test
## Description

This sample project is managing gateways - master devices that control multiple peripheral devices. 
Your task is to create a REST service (JSON/HTTP) for storing information about these gateways and their associated devices. This information must be stored in the database. 
When storing a gateway, any field marked as �to be validated� must be validated and an error returned if it is invalid. Also, no more than 10 peripheral devices are allowed for a gateway.
The service must also offer an operation for displaying information about all stored gateways (and their devices) and an operation for displaying details for a single gateway.
 Finally, it must be possible to add and remove a device from a gateway.
 
 ### Technology
* Programming Language Java (version 8 and above)
* Remote Service Protocol REST
* Testing/ Stubbing Junit, Mockito
* Build Maven
*Framework: Spring Boot
*Database: MySQL
*Automated build: Gitlab CI/CD

## Other considerations
* all most done except UI as a frontend
    *  Basic UI - provided postman collection file : Musala.postman_collection.json and also adding swagger config. on : http://localhost:8080/swagger-ui.html
    *  Meaningful Unit tests. ( done )
    *  Readme file with installation guides.( done )
    *  An automated build. ( done using gitlab CI/CD )

## How to run

* using compilers
    * make sure that java version > java 8 is installed
	* clone Repository from gitlab : https://gitlab.com/EslamElkhafagy/musala/-/tree/master
	* make sure that maven is installed and run ```mvn clean install```
	* Running the Project
	

