package com.example.musala.service;

import com.example.musala.dao.PeripheralDeviceRepository;
import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.Gateway;
import com.example.musala.model.PeripheralDevice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;

/**
 * Test Peripheral Device Service
 * naming convention : methodName_input_output
 */
@SpringBootTest(classes = PeripheralDeviceServiceTest.class)
@RunWith(SpringRunner.class)
public class PeripheralDeviceServiceTest {

    private final int VALID_DEVICES_FOR_GATEWAY=5;
    private final int INVALID_DEVICES_FOR_GATEWAY=11;


    @InjectMocks
    private PeripheralDeviceServiceImpl deviceService;

    @Mock
    private PeripheralDeviceRepository deviceRepository;


    @Test
    public void createPeripheralDevice_device_device() throws BusinessExcption {

        Mockito.doReturn(VALID_DEVICES_FOR_GATEWAY).when(deviceRepository).countByGatewayId(Matchers.anyLong());
        Mockito.doReturn(getPeripheralDevice()).when(deviceRepository).save(Matchers.anyObject());

        deviceService.createPeripheralDevice(getPeripheralDevice());
    }

    @Test(expected = BusinessExcption.class)
    public void createPeripheralDevice_device_internalServer() throws BusinessExcption {

        Mockito.doReturn(VALID_DEVICES_FOR_GATEWAY).when(deviceRepository).countByGatewayId(Matchers.anyLong());
        Mockito.doReturn(null).when(deviceRepository).save(Matchers.anyObject());

        deviceService.createPeripheralDevice(getPeripheralDevice());
    }
    @Test(expected = BusinessExcption.class)
    public void createPeripheralDevice_device_invalidNumberOfDevices() throws BusinessExcption {

        Mockito.doReturn(INVALID_DEVICES_FOR_GATEWAY).when(deviceRepository).countByGatewayId(Matchers.anyLong());
        Mockito.doReturn(getPeripheralDevice()).when(deviceRepository).save(Matchers.anyObject());

        deviceService.createPeripheralDevice(getPeripheralDevice());
    }

    @Test
    public void deletePeripheralDevice_device_device() throws BusinessExcption {

        Mockito.doNothing().when(deviceRepository).delete(Matchers.anyObject());

        deviceService.deletePeripheralDevice(getPeripheralDevice());
    }
    private PeripheralDevice getPeripheralDevice(){

        PeripheralDevice device= new PeripheralDevice();
        device.setId(5l);
        device.setUid(123456789105l);
        device.setCreationDate(new Date());
        device.setStatus("online");
        device.setVendor("Vendor");
        device.setGateway(getGateway());
        return device;
    }

    private Gateway getGateway(){
        Gateway gateway= new Gateway();
        gateway.setHumanReadable("Human");
        gateway.setSerialNumber("ES12302254");
        gateway.setIpv4Address("10.20.30.40");
        gateway.setId(5l);
        return gateway;

    }
}
