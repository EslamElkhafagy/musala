package com.example.musala.service;

import com.example.musala.dao.GatewayRepository;
import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.Gateway;
import com.example.musala.model.PeripheralDevice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Test Gateway Service
 * naming convention : methodName_input_output
 */
@SpringBootTest(classes = GatewayServiceTest.class)
@RunWith(SpringRunner.class)
public class GatewayServiceTest {

    private final String VALID_IPV4="10.52.25.2";
    private final String INVALID_IPV4="10.25.2";

    @InjectMocks
    private GatewayServiceImpl gatewayService;

    @Mock
    private GatewayRepository gatewayRepository;


    @Test
    public void createGateway_gateway_gateway() throws Exception {
        Mockito.doReturn(getGateway(VALID_IPV4)).when(gatewayRepository).save(Matchers.anyObject());

        gatewayService.createGateway(getGateway(VALID_IPV4));

    }

    @Test(expected = BusinessExcption.class)
    public void createGateway_gateway_invalidIPV4() throws Exception {
        Mockito.doReturn(getGateway(VALID_IPV4)).when(gatewayRepository).save(Matchers.anyObject());

        gatewayService.createGateway(getGateway(INVALID_IPV4));

    }

    @Test(expected = BusinessExcption.class)
    public void createGateway_gateway_internalServer() throws Exception {
        Mockito.doReturn(null).when(gatewayRepository).save(Matchers.anyObject());

        gatewayService.createGateway(getGateway(VALID_IPV4));

    }

    @Test
    public void gatewayBySerialNumber_serialNumber_gateway() throws BusinessExcption {
        Optional<Gateway> optionalGateway=Optional.of(getGateway(VALID_IPV4));

        Mockito.doReturn(optionalGateway).when(gatewayRepository).findBySerialNumber(Matchers.anyString());

    gatewayService.getGatewayBySerialNumber("ES12345");
    }

    @Test(expected = NullPointerException.class)
    public void gatewayBySerialNumber_serialNumber_NullPointer() throws BusinessExcption {
        Optional<Gateway> optionalGateway=Optional.of(null);

        Mockito.doReturn(optionalGateway).when(gatewayRepository).findBySerialNumber(Matchers.anyString());

        gatewayService.getGatewayBySerialNumber("ES12345");
    }


    @Test
    public void getAllGateway_non_gatewayList()throws BusinessExcption{

        Mockito.doReturn(getGatewayList()).when(gatewayRepository).findAll();

        gatewayService.getAllGateway();
    }

    @Test(expected = NullPointerException.class)
    public void getAllGateway_non_null()throws BusinessExcption{

        Mockito.doReturn(null).when(gatewayRepository).findAll();

        gatewayService.getAllGateway();
    }
    @Test(expected = BusinessExcption.class)
    public void getAllGateway_non_internalServer()throws BusinessExcption{
        List<Gateway> gatewayList= new ArrayList<>();
        gatewayList.add(null);

        Mockito.doReturn(gatewayList).when(gatewayRepository).findAll();

        gatewayService.getAllGateway();
    }
    private List<Gateway> getGatewayList(){
        List<Gateway> gatewayList= new ArrayList<>();
        gatewayList.add(getGateway(VALID_IPV4));
        return gatewayList;
    }
    private Gateway getGateway(String ip){
        Gateway gateway= new Gateway();
        gateway.setPeripheralDevice(getPeripheralDevices());
        gateway.setHumanReadable("Human");
        gateway.setSerialNumber("ES12302254");
        gateway.setIpv4Address(ip);
        return gateway;

    }

    private List<PeripheralDevice> getPeripheralDevices(){

        List<PeripheralDevice> peripheralDeviceList= new ArrayList<>();

        PeripheralDevice device= new PeripheralDevice();
        device.setUid(123456789105l);
        device.setCreationDate(new Date());
        device.setStatus("online");
        device.setVendor("Vendor");

        peripheralDeviceList.add(device);

        return peripheralDeviceList;
    }

}
