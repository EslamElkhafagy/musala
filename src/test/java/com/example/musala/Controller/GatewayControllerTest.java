package com.example.musala.Controller;

import com.example.musala.controller.GatewayController;
import com.example.musala.model.Gateway;
import com.example.musala.model.PeripheralDevice;
import com.example.musala.service.GatewayServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Test Gateway Controller
 * naming convention : methodName_input_output
 */
@SpringBootTest(classes = GatewayControllerTest.class)
@RunWith(SpringRunner.class)
public class GatewayControllerTest {

    private final String BASE_URL="/api/gateway/";
    @InjectMocks
    private GatewayController gatewayController;

    @Mock
    private GatewayServiceImpl gatewayService;

    MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(gatewayController).build();
    }


    @Test
    public void createGateway_gateway_gateway() throws Exception {
        ObjectMapper objectMapper= new ObjectMapper();

    Mockito.doReturn(getGatewayResponse()).when(gatewayService).createGateway(Matchers.anyObject());
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new Gateway())))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAllGateway_noThing_listGateway() throws Exception {
        Mockito.doReturn(getGatewayListResponse()).when(gatewayService).getAllGateway();
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isFound());
    }

    @Test
    public void getGatewayBySerialNumber_serialNumber_gateway() throws Exception {
        String serialNumber = "ES12345";
        Mockito.doReturn(getGatewayResponse()).when(gatewayService).getGatewayBySerialNumber(Matchers.anyString());
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL+"{serialNumber}",serialNumber)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    private ResponseEntity<List<Gateway>> getGatewayListResponse(){
        List<Gateway> gatewayList= new ArrayList<>();
        gatewayList.add(getGateway());
        return new ResponseEntity<>(gatewayList,HttpStatus.FOUND);
    }
    private Gateway getGateway(){
        Gateway gateway= new Gateway();
        gateway.setPeripheralDevice(getPeripheralDevices());
        gateway.setHumanReadable("Human");
        gateway.setSerialNumber("ES12302254");
        gateway.setIpv4Address("10.52.25.2");
        return gateway;

    }

    private ResponseEntity<Gateway> getGatewayResponse(){

        return new ResponseEntity<Gateway>(getGateway(), HttpStatus.CREATED);

    }

    private List<PeripheralDevice> getPeripheralDevices(){

        List<PeripheralDevice> peripheralDeviceList= new ArrayList<>();

        PeripheralDevice device= new PeripheralDevice();
        device.setUid(123456789105l);
        device.setCreationDate(new Date());
        device.setStatus("online");
        device.setVendor("Vendor");

        peripheralDeviceList.add(device);

        return peripheralDeviceList;
    }

}
