package com.example.musala.Controller;

import com.example.musala.controller.PeripheralDeviceController;
import com.example.musala.model.PeripheralDevice;
import com.example.musala.service.PeripheralDeviceServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Date;

/**
 * Test Peripheral Device Controller
 * naming convention : methodName_input_output
 */
@SpringBootTest(classes = PeripheralDeviceControllerTest.class)
@RunWith(SpringRunner.class)
public class PeripheralDeviceControllerTest {

    private final String BASE_URL="/api/device/";
    @InjectMocks
    private PeripheralDeviceController deviceController;

    @Mock
    private PeripheralDeviceServiceImpl deviceService;

    MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(deviceController).build();
    }


    @Test
    public void createDevice_device_device() throws Exception {
        ObjectMapper objectMapper= new ObjectMapper();

        Mockito.doReturn(getDeviceResponse(HttpStatus.CREATED)).when(deviceService).createPeripheralDevice(Matchers.anyObject());
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new PeripheralDevice())))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }



    @Test
    public void deleteDevice_device_device() throws Exception {
        ObjectMapper objectMapper= new ObjectMapper();

        Mockito.doReturn(getDeviceResponse(HttpStatus.OK)).when(deviceService).deletePeripheralDevice(Matchers.anyObject());
        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PeripheralDevice())))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private PeripheralDevice getDevice(){
        PeripheralDevice device= new PeripheralDevice();
        device.setUid(123456789105l);
        device.setCreationDate(new Date());
        device.setStatus("online");
        device.setVendor("Vendor");
        return device;

    }

    private ResponseEntity<PeripheralDevice> getDeviceResponse(HttpStatus httpStatus){

        return new ResponseEntity<PeripheralDevice>(getDevice(), httpStatus);

    }




}
