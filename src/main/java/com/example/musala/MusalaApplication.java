package com.example.musala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusalaApplication.class, args);
	}

}
