package com.example.musala.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.ExceptionHandler;
@ControllerAdvice
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(BusinessExcption.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> processMethodNotSuppotredException(BusinessExcption e,WebRequest request){
        ErrorResponse errorResponse= new ErrorResponse();

        errorResponse.setMessage(e.getMessage());
        errorResponse.setHttpStatus(e.getHttpStatus());
        return new ResponseEntity<ErrorResponse>(errorResponse,e.getHttpStatus());
    }

}
