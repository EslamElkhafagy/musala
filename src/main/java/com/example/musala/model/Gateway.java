package com.example.musala.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Gateway {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String serialNumber;
    private String humanReadable;
    private String ipv4Address;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true,mappedBy = "gateway")
    private List<PeripheralDevice> peripheralDevice;
}
