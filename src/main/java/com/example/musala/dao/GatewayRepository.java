package com.example.musala.dao;

import com.example.musala.model.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GatewayRepository extends JpaRepository<Gateway,Long> {


      Optional<Gateway> findById(Long id);
      Optional<Gateway> findBySerialNumber(String serialNumber);


}
