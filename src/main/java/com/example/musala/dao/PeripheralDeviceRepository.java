package com.example.musala.dao;


import com.example.musala.model.PeripheralDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface PeripheralDeviceRepository extends JpaRepository<PeripheralDevice,Long> {
    @Query(value = "SELECT COUNT(*) FROM musala.peripheral_device de WHERE de.gateway_id=?",nativeQuery = true)
    int countByGatewayId(Long id);

}
