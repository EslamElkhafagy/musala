package com.example.musala.controller;


import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.Gateway;
import com.example.musala.service.GatewayServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/gateway/")
@Slf4j
public class GatewayController {

    @Autowired
    private GatewayServiceImpl gatewayService;


    @PostMapping("")
    public ResponseEntity<Gateway> createGateway(@RequestBody Gateway gateway)throws BusinessExcption {
    log.info("creating new gateway: "+gateway.toString());

    return gatewayService.createGateway(gateway);

    }

    @GetMapping("")
    public ResponseEntity<List<Gateway>> getAllGateways()throws BusinessExcption{

        return gatewayService.getAllGateway();

    }

    @GetMapping("{serialNumber}")
    public ResponseEntity<Gateway> getGatewayBySerialNumber(@PathVariable("serialNumber") String serialNumber)throws BusinessExcption{

        return gatewayService.getGatewayBySerialNumber(serialNumber);

    }

}
