package com.example.musala.controller;

import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.PeripheralDevice;
import com.example.musala.service.PeripheralDeviceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/device/")
@Slf4j
public class PeripheralDeviceController {


    @Autowired
    private PeripheralDeviceServiceImpl deviceService;

    @PostMapping("")
    public ResponseEntity<PeripheralDevice> createDevice(@RequestBody PeripheralDevice device)throws BusinessExcption{
        log.info("creating new device: "+device.toString());

        return deviceService.createPeripheralDevice(device);

    }


    @DeleteMapping("")
    public ResponseEntity<PeripheralDevice> deleteDevice(@RequestBody PeripheralDevice device)throws BusinessExcption {
        log.info("deleting device: "+device.toString());

        return deviceService.deletePeripheralDevice(device);

    }


}
