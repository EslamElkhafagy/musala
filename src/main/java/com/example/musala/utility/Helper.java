package com.example.musala.utility;

import com.example.musala.exceptions.BusinessExcption;
import org.springframework.http.HttpStatus;

public class Helper {


    public static void validObject(Object object) throws BusinessExcption{
        if (object == null)
            throw  new BusinessExcption("object is null, please check your request and data", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
