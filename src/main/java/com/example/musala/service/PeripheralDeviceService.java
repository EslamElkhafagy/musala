package com.example.musala.service;

import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.PeripheralDevice;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

public interface PeripheralDeviceService {

    ResponseEntity<PeripheralDevice> createPeripheralDevice(PeripheralDevice peripheralDevice)throws BusinessExcption;
    ResponseEntity<PeripheralDevice> deletePeripheralDevice(PeripheralDevice peripheralDevice)throws BusinessExcption;

}
