package com.example.musala.service;

import com.example.musala.dao.GatewayRepository;
import com.example.musala.dao.PeripheralDeviceRepository;
import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.Gateway;
import com.example.musala.utility.Helper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.List;

@Slf4j
@Service
public class GatewayServiceImpl implements GatewayService {

    @Autowired
    private GatewayRepository gatewayRepository;

    @Autowired
    private PeripheralDeviceRepository deviceRepository;
    @Override
    public ResponseEntity<Gateway> createGateway(Gateway gateway) throws BusinessExcption{
        String message="";
        if(!isValidInet4Address(gateway.getIpv4Address())) {
             message="IPV4 is not valid";
            log.error(message+" for "+gateway.getSerialNumber() );
        throw new BusinessExcption(message,HttpStatus.BAD_REQUEST);
        }
        Gateway savedGateway=null;
        try {
             savedGateway = gatewayRepository.save(gateway);
            log.info("gateway saved with id :" + savedGateway.getId());
        }catch (Exception e){
            message="Error occurred while saving gateway or may be exist";
            log.error(message);
        throw new BusinessExcption(message,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(savedGateway, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Gateway> getGatewayBySerialNumber(String serialNummber)throws BusinessExcption {
        Gateway gateway= gatewayRepository.findBySerialNumber(serialNummber).get();

        Helper.validObject(gateway);
        return new ResponseEntity<>(gateway,HttpStatus.FOUND);
    }

    @Override
    public ResponseEntity<List<Gateway>> getAllGateway() throws BusinessExcption{

        List<Gateway> allGateways=gatewayRepository.findAll();
        Helper.validObject(allGateways.get(0));

        return new ResponseEntity<>(allGateways,HttpStatus.FOUND);
    }


    private boolean isValidInet4Address(String ip)
    {
        try {
            return Inet4Address.getByName(ip)
                    .getHostAddress().equals(ip);
        }
        catch (UnknownHostException ex) {
            return false;
        }
    }


}
