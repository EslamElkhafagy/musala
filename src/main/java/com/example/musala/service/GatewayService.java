package com.example.musala.service;


import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.Gateway;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface GatewayService {

    ResponseEntity<Gateway> createGateway(Gateway gateway)throws BusinessExcption;
    ResponseEntity<Gateway> getGatewayBySerialNumber(String serialNummber)throws BusinessExcption;
    ResponseEntity<List<Gateway>> getAllGateway()throws BusinessExcption;


}
