package com.example.musala.service;

import com.example.musala.dao.PeripheralDeviceRepository;
import com.example.musala.exceptions.BusinessExcption;
import com.example.musala.model.PeripheralDevice;
import com.example.musala.utility.Helper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class PeripheralDeviceServiceImpl implements  PeripheralDeviceService{

    @Autowired
    private PeripheralDeviceRepository deviceRepository;
    @Override
    public ResponseEntity<PeripheralDevice> createPeripheralDevice(PeripheralDevice peripheralDevice) throws BusinessExcption {

        int count=deviceRepository.countByGatewayId(peripheralDevice.getGateway().getId());
        log.info("#of devices for this gateway : "+count);
        if (count>9){
             throw new BusinessExcption("can not add more than 10 device for the same gateway",HttpStatus.BAD_REQUEST);
        }

        PeripheralDevice device=null;
        try {
            peripheralDevice.setCreationDate(new Date());
            device = deviceRepository.save(peripheralDevice);
            log.info("PeripheralDevice saved with id :" + peripheralDevice.getId() + " , for gateway serialNumber :" + peripheralDevice.getGateway().getSerialNumber());
            Helper.validObject(device);
        }catch (Exception e){
            throw new BusinessExcption(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(device, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<PeripheralDevice> deletePeripheralDevice(PeripheralDevice peripheralDevice)throws BusinessExcption {
        try {
            deviceRepository.delete(peripheralDevice);
        }catch (Exception e){
            throw new BusinessExcption(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(peripheralDevice,HttpStatus.OK);
    }

}
